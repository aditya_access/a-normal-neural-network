from keras.models import Sequential
from keras.layers import Dense
import random
from random import seed
# import tensorflow
import numpy as np

seed(1)


def get_dataset(n_samples):
    for i in range(n_samples):
        if i % 2 == 0:
            X.append([random.uniform(-0.1, -0.2), random.uniform(0.1, 0.2), random.uniform(-0.1, -0.2),
            random.uniform(0.3, 0.5), random.uniform(-0.3, -0.5), random.uniform(1.5, 2.5),
            random.uniform(0.5, 1.5), random.uniform(0.5, 1.5), random.uniform(0.8, 1.0)])
            y.append(1)
        else:
            X.append([random.uniform(-0.05, -0.15), random.uniform(0.05, 0.15), random.uniform(-0.05, -0.15),
            random.uniform(0.3, 0.5), random.uniform(-0.3, -0.5), random.uniform(1, 2),
            random.uniform(0.1, 1), random.uniform(0.1, 1), random.uniform(0.5, 0.8)])
            y.append(0)
    return np.array(X), np.array(y)


def main():
    X, y = get_dataset(40)
    test_X, test_y = get_dataset(10)
    model = Sequential()
    model.add(Dense(12, input_dim=9, activation='relu'))
    model.add(Dense(8, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.fit(X, y, epochs=150, batch_size=10, verbose=0)
    predictions = model.predict_classes(test_X)
    for i in range(10):
        print('{0} => {1} (expected {2})'.format(test_X[i].tolist(), predictions[i], test_y[i]))
        # print('%s => %d (expected %d)' % (test_X[i].tolist(), predictions[i], test_y[i]))

main()